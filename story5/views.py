from django.shortcuts import render, redirect
from .models import MataKuliah
from .forms import MatkulForm
from django.contrib import messages

def index(request):
    context = {
        "mata_kuliah_list" : MataKuliah.objects.all().order_by("-id"),
    }
    return render(request,"index.html",context)

def add_matkul(request):
    if request.method == "POST":
        matkul_form = MatkulForm(request.POST)
        if matkul_form.is_valid():
            matkul_form.save()
            messages.add_message(request, messages.SUCCESS, "Matkul '{}' Ditambahkan".format(request.POST["nama"]))
        return redirect("story5:index")
    return render(request,"add_matkul.html")

def matkul_detail(request,id):
    matkul = MataKuliah.objects.get(id=id)
    context = {
        "mata_kuliah" : matkul,
    }
    return render(request,"detail_matkul.html",context)

def delete_matkul(request,id):
    if request.method == "POST":
        mata_kuliah = MataKuliah.objects.get(id=id)
        mata_kuliah.delete()
        messages.add_message(request, messages.SUCCESS, "Matkul '{}' Dihapuskan".format(mata_kuliah.nama))
    return redirect("story5:index")

# Create your views here.
