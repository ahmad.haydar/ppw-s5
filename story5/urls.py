from django.urls import path
from . import views
from .views import index, add_matkul, matkul_detail, delete_matkul

app_name = "story5"

urlpatterns = [
    path("",views.index,name="index"),
    path("add_matkul/",views.add_matkul,name="add_matkul"),
    path("matkul/<int:id>",views.matkul_detail,name="matkul_detail"),
    path("delete_matkul/<int:id>",views.delete_matkul,name="delete_matkul")
]
