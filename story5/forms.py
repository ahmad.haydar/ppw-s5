from django import forms
from .models import MataKuliah

class MatkulForm(forms.ModelForm):
    class Meta:
        model = MataKuliah
        fields = "__all__"
